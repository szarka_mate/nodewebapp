
var orderModel = {
    _id: Object,
    userId:String,
    placedAt: Date,
    items:Array
}

module.exports = orderModel;