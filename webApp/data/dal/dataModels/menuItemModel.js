
var menuItem = {
    _id: Object,
    name: String,
    description: String,
    price: Number,
    spicy: Boolean,
    Vegetarian: Boolean
}
module.exports = menuItem;