
var userModel = {
    _id: Object,
    tokenId: Object,
    role:String,
    shoppingCartId:String,
    email: String,
    password: String,
    registeredAt: Date,
}