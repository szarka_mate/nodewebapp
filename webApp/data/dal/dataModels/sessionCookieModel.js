var sessionCookieModel = {
    name:String,
    userId: String,
    accessToken: String,
    accessTokenSecret:String
}