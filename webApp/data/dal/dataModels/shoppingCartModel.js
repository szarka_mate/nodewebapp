var menuItem = require('./menuItemModel');
var shoppingCartModel = {
    _id: Object,
    userId: String,
    items: [menuItem]
};

module.exports = shoppingCartModel;