const express = require('express');
const router = express.Router();
const orderManager = require('../../models/BusinessLogic/ordersManager');
const userManager = require('../../models/BusinessLogic/userManager');
const cookieManager = require('../../models/BusinessLogic/cookieManager');

router.all('/*', function (req, res, next) {
    if (!req.cookies.sessionCookie)
        return res.redirect('../account/login')

    userManager.authorize(req.cookies.sessionCookie)
        .then(() => {
            next();
    })

});

router.get('/', function(req, res, next) {
    const userId = ObjectId(cookieManager.getDecodedCookie(req.cookies.sessionCookie).userId);
    orderManager.getOrdersByUserId(userId)
        .then(result => {
            res.render('order/orderList', { title: 'Orders', data: result });
        })
        .catch(err => {
            return next(err);
        });
});

module.exports = router;
