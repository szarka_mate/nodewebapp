var express = require('express');
var router = express.Router();
var shoppingCartManager = require('../../models/BusinessLogic/shoppingCartManager');
var ordersManager = require('../../models/BusinessLogic/ordersManager');
var userManager = require('../../models/BusinessLogic/userManager');
var jwt = require('jsonwebtoken');
var cookieManager = require('../../models/BusinessLogic/cookieManager');
ObjectId = require('mongodb').ObjectID;


router.all('/*', function (req, res, next) {
    try {
        userManager.authorize(req.cookies.sessionCookie);
        next();
    } catch {
        res.redirect('../account/login', { title: 'Login' });
    }
});

/* GET cart main */
router.get('/', function (req, res, next) {
    const userId = ObjectId(cookieManager.getDecodedCookie(req.cookies.sessionCookie).userId);
    shoppingCartManager
        .getByUserId(userId)
        .then(result => {
            let sumPrice = 0;
            result.items.forEach(x => sumPrice += x.price * x.count);
            res.render('cart/cartList', { title: 'ShoppingCart', data: result.items, sumPrice:sumPrice });
        })
        .catch(err => {
            return next(err);
        });
});

router.delete('/removeItem/:id', function(req, res) {
    const itemToRemove = req.params.id;
    const userId = ObjectId(cookieManager.getDecodedCookie(req.cookies.sessionCookie).userId);
    shoppingCartManager
        .removeFromUserCartById(userId, itemToRemove)
        .then(result => {
            let sumPrice = 0;
            result.items.forEach(x => sumPrice += x.count * x.price)
            res.send({ msg: '' ,item:result.items.find(x => x._id.toString() === itemToRemove), sumPrice:sumPrice});
        })
        .catch(err => {
            res.send({ msg: 'Error: ' + err });
        });
});

router.delete('/emptyCart', function (req, res) {
    const userId = ObjectId(cookieManager.getDecodedCookie(req.cookies.sessionCookie).userId);
    shoppingCartManager
        .emptyCartForUser(userId)
        .then(() => {
            res.send({ msg: '' });
        })
        .catch(err => {
            res.send({ msg: 'Error: ' + err });
        });
});

router.post(`/placeOrder`, function (req, res, next) {
    const userId = ObjectId(cookieManager.getDecodedCookie(req.cookies.sessionCookie).userId);
    ordersManager.plcaeOrder(userId)
        .then(() => {
            res.send({ msg: '' });
        })
        .catch(err => {
            return next(err);
        });
});
function getUserIdFromCookie(cookie) {
    var dec = jwt.verify(cookie, 'secret');
    return dec.userId;
}

module.exports = router;
