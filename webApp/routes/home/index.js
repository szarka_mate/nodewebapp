var express = require('express');
var router = express.Router();
var menuItemsManager = require('../../models/BusinessLogic/menuItemsManager.js');
var shoppingCartManager = require('../../models/BusinessLogic/shoppingCartManager');
var userManager = require('../../models/BusinessLogic/userManager');
var cookieManager = require('../../models/BusinessLogic/cookieManager')
ObjectId = require('mongodb').ObjectID;

/* GET home page. */
router.get('/', function(req, res, next) {
    menuItemsManager
        .getAll()
        .then(data => {
            res.render('home/index', { title: 'Home', data: data });
        })
        .catch(msg => {
            return next(msg);
        });
});

router.get('/addToCart/:id&count=:count', function(req, res, next) {
    const itemId = req.params.id;
    const itemCount = req.params.count;
    menuItemsManager
        .getItemById(itemId).then(result => {
            shoppingCartManager.addToCartByUserId(ObjectId(cookieManager.getDecodedCookie(req.cookies.sessionCookie).userId), result, itemCount);
        }).then(result => {
            res.send({ msg: '' });
        }).catch(err => {
            res.send({ msg: 'Error: ' + err });
        });
});

// router.get("/helloworld", function(req, res) {
//     res.render("helloworld", { title: "Hello, World!" });
// });

// router.get("/userlist", function(req, res) {
//     var db = req.db;
//     var collection = db.get("usercollection");
//     collection.find({}, {}, function(e, docs) {
//         res.render("userlist", { userlist: docs });
//     });
// });

// router.get('/newuser', function(req, res) {
//   res.render('newuser', {title: 'Add New User'});
// });

// router.post("/adduser", function(req, res) {
//     var db = req.db;
//     var userName = req.body.username;
//     var userEmail = req.body.useremail;

//     var collection = db.collection("usercollection");

//     collection.insert(
//         {
//             username: userName,
//             email: userEmail,
//         },
//         function(err, doc) {
//             if (err) {
//                 res.send("There was a problem adding the information to the database");
//             } else {
//                 res.redirect("userlist");
//             }
//         }
//     );
// });

module.exports = router;
