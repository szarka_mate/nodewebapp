const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const userManager = require('../../models/BusinessLogic/userManager');
const fs = require('fs')

router.get('/', function (req, res, next) {
    var token = req.cookies['sessionCookie'];
    if (!token) {
        fs.readFile(__dirname + '../../../views/shared/partials/loginPartial.hbs', 'utf-8', (err, data) => {
            if (err)
                return { msg: 'Err: ' + err };

            return res.send({ msg: '', page: data });
        });
    } else {

        var decToken = jwt.verify(token, 'secret');
        userManager.getUserByTokenId(decToken.accessToken).then(result => {
            if (!result) {
                return { msg: 'Cant find user with tokenId' };
            }
            fs.readFile(__dirname + '../../../views/shared/partials/loginPartial.hbs', 'utf-8', (err, data) => {
                if (err)
                    return { msg: 'Err: ' + err };

                res.send({ msg: '', email: result.email, page: data });
            })
        }).catch(err => {
            return next(err);
        });
    }
});

module.exports = router;