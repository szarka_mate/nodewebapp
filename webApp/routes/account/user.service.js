function auth(req, res, next) {
    console.table(req.headers);
    if (req.path === 'account/login') {
        return next();
    }

    if (!req.headers.authorization || req.headers.authorization.indexOf('Basic ') === -1) {
        res.setHeader('WWW-Authenticate', 'Basic realm="access to page"', 'charset ="utf-8"');
        return res.status(401).json({ msg: 'Missing authorization header' });
    }

    const base64Credentials = req.headers.authorization.split(' ')[1];
    const credentials = Buffer.from(base64Credentials, 'base64').toString('utf-8');
    const [email, password] = credentials.split(':');
    const userCollection = req.db.collection('Users');
    userCollection.find({ email: email, password: password }, {}, function(result) {
        if (!result[0]) {
            return res.status(401).json({ msg: 'Invalid authentication credentials' });
        }
        req.user = result[0];
        return next();
    });
}

module.exports = auth;
