var express = require('express');
var router = express.Router();
var userManager = require('../../models/BusinessLogic/userManager');
var cartManager = require('../../models/BusinessLogic/shoppingCartManager');
var guidHelper = require('../../models/BusinessLogic/helpers/guidHelper');
var jwt = require('jsonwebtoken');

router.get('/login', function(req, res, next) {
    res.render('account/login', { title: 'Login' });
});

router.post('/login', function(req, res, next) {
    const sessionId = guidHelper.createGuid();
    userManager
        .signIn(req.body.email, req.body.password)
        .then(result => {
            if (result.msg !== '') {
                return next(result.msg);
            }
            result.user.tokenId = sessionId;
            return userManager.updateUser(result.user);
        })
        .then(result => {
            const jwtPayload = {
                userEmail: result.email,
                userId: result._id,
                accessToken: result.tokenId,
            };
            const authJwtToken = jwt.sign(jwtPayload, 'secret');
            const cookieOptions = {
                httpOnly: true,
                expires: 0,
            };
            res.cookie('sessionCookie', authJwtToken, cookieOptions);
            console.log('cookies added');
            res.redirect('/');
        })
        .catch(err => {
            return next(err);
        });
});

router.get('/logout', function(req, res, next) {
    res.cookie('sessionCookie', '', { expires: new Date() });
    res.redirect('/');
});

router.get('/register', function(req, res, next) {
    res.render('account/register', { title: 'Register' });
});

router.post('/register', function(req, res, next) {
    var date = new Date();
    userManager
        .createUser({
            email: req.body.email,
            password: req.body.password,
            registeredAt: date.toString(),
            shoppingCartId: '',
            tokenId: '',
        })
        .then(user => {
            console.log('User created: ' + user.toString());
            return cartManager.createCart({ userId: user._id, items: [] });
        })
        .then(cart => {
            console.log('cart created: ' + cart.toString());
            return userManager.getUserByEmail(req.body.email).then(user => {
                user.shoppingCartId = cart._id;
                return userManager.updateUser(user);
            });
        })
        .then(() => {
            console.log('registration done');
            res.redirect('./login');
        })
        .catch(err => {
            console.log('oops error: ' + err.toString());
            return next(err);
        });
});

module.exports = router;
