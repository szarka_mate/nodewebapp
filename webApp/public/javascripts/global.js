$(document).ready(function() {
    $('#addToCartButton').click(addToCart);
    $('.linkdeleteitem').click(removeItemFromCart);
    $('#emptyCartButton').click(emptyCart);
    $('#placeOrderButton').click(placeOrder);

    getMe();
    //$('registerButton').click(login);
    //$('loginButton').click(register);
});

function getMe() {
    $.ajax({
        type: 'GET',
        url: '/me',
    }).done(function(response) {
        if (response.msg === '') {
            var email = response.email;

            var stuff = Handlebars.compile(response.page);
            var context = { email: email };
            var html = stuff(context);
            $('#loginPart').html(html);
        }
    });
}

function addToCart(event) {
    event.preventDefault();
    var itemToAdd = $(this).attr('rel');

    $.ajax({
        type: 'GET',
        url: `/addToCart/${itemToAdd}&count=1`,
    }).done(function(response) {
        if (response.msg !== '') {
            alert('Error: ' + response.msg);
        }
    });
}
function removeItemFromCart(event) {
    event.preventDefault();
    var itemToDelete = $(this).attr('rel');

    $.ajax({
        type: 'DELETE',
        url: '/cart/removeItem/' + itemToDelete,
    }).done(function(response) {
        if (response.msg === '') {
            const item = response.item;
            if (item) {
                $(`#${itemToDelete} #count`).html(item.count);
                $(`#${itemToDelete} #total`).html(item.totalPrice);
                $('#sumPrice').html("Sum price: " + response.sumPrice)
            } else {
                $('#' + itemToDelete).empty();
                $('#sumPrice').html("Sum price: 0")
            }
        } else {
            alert('Error:' + response.msg);
        }
    });
}

function emptyCart(event) {
    event.preventDefault();

    $.ajax({
        type: 'DELETE',
        url: '/cart/emptyCart/',
    }).done(function(response) {
        if (response.msg === '') {
            $('table tbody').empty();
            window.location = '/';
        } else {
            alert('Error: ' + response.msg);
        }
    });
}

function placeOrder(event) {
    event.preventDefault();
    $.ajax({
        type: `POST`,
        url: `cart/placeOrder`,
    }).done(function(response) {
        console.log(response);
        if (response.msg === '') {
            window.location = '/orders';
        } else {
            alert('Error: ' + response.msg);
        }
    });
}

function showUserInfo(event) {
    event.preventDefault();

    var thisUserName = $(this).attr('rel');
    var arrayPosition = userListData
        .map(function(arrayItem) {
            return arrayItem.username;
        })
        .indexOf(thisUserName);

    var thisUserObject = userListData[arrayPosition];

    $('#userInfoName').text(thisUserObject.fullname);
    $('#userInfoAge').text(thisUserObject.age);
    $('#userInfoGender').text(thisUserObject.gender);
    $('#userInfoLocation').text(thisUserObject.location);
}

function addUser(event) {
    event.preventDefault();

    var errorCount = 0;
    $('#addUser input').each(function(index, val) {
        if ($(this).val() === '') {
            errorCount++;
        }
    });

    if (errorCount === 0) {
        var newUser = {
            username: $('#addUser fieldset input#inputUserName').val(),
            email: $('#addUser fieldset input#inputUserEmail').val(),
            fullname: $('#addUser fieldset input#inputUserFullname').val(),
            age: $('#addUser fieldset input#inputUserAge').val(),
            location: $('#addUser fieldset input#inputUserLocation').val(),
            gender: $('#addUser fieldset input#inputUserGender').val(),
        };

        $.ajax({
            type: 'POST',
            data: newUser,
            url: `/users/adduser/${$(this).attr('rel')}`,
            datatype: 'JSON',
        }).done(function(response) {
            if (response.msg === '') {
                $('#addUser fieldset input').val('');
                populateTable();
            } else {
                alert('Error' + response.msg);
            }
        });
    } else {
        alert('Please fill in all fields');
        return false;
    }
}
