var createError = require('http-errors');
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var exphbs = require('express-handlebars');
var bcrypt = require('bcryptjs');
const secretKey = 'secretkey123456';

// db
var mongo = require('mongodb');
var monk = require('monk');
var db = monk('localhost:27017/nodeWebApp');

var indexRouter = require('./routes/home/index');
var cartRouter = require('./routes/cart/cartList');
var orderRouter = require('./routes/order/orderList');
var usersRouter = require('./routes/account/users');
var meRouter = require('./routes/account/me');



var app = express();

// view engine setup


app.set('views', path.join(__dirname, 'views'));
app.engine('hbs', exphbs({
    extname: '.hbs',
    defaultLayout: 'main',
    layoutsDir: __dirname + '/views/shared',
    partialsDir: __dirname + '/views/shared/partials'
}));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(cookieParser());
// grants db access to router
app.use(function(req, res, next) {
    req.db = db;
    next();
});
app.use('/', indexRouter);
app.use('/cart', cartRouter);
app.use('/account', usersRouter);
app.use('/orders', orderRouter);
app.use('/me', meRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('shared/error', { title: 'Error' });
});

module.exports = app;
