var db = require('../../data/dal/databaseAccess.js');
var orders = db.collection('OrderList');
var cartManager = require('./shoppingCartManager');

var orderManager = {
    getAll: function() {
        return orders.find({});
    },
    getOrderById: function(id) {
        return orders.findOne({ _id: id });
    },
    getOrdersByUserId: function(userId) {
        return orders.find({ userId: userId })
    },
    createOrder: function(newOrder) {
        return orders.insert(newOrder);
    },
    createOrderFromCart(cart) {
        var date = new Date();
        let sumPrice = 0;
        cart.items.forEach(x => sumPrice += x.totalPrice)
        var order = {
            userId: cart.userId,
            placedAt: date.toISOString(),
            items: cart.items,
            sumPrice: sumPrice
        }
        return orderManager.createOrder(order);
    },

    updateOrder: function(updatedOrder) {
        return orders.findOneAndUpdate({ _id: updatedOrder._id }, updatedOrder)
    },

    plcaeOrder: function (userId) {
        return cartManager.getByUserId(userId)
            .then(result => {
                if (result.items.length > 0) {
                    return orderManager.createOrderFromCart(result);
                }
            })
            .then(() => {
                return cartManager.emptyCartForUser(userId);
            });
    }
};

module.exports = orderManager;
