var db = require('../../data/dal/databaseAccess.js');
var users = db.collection('Users');
var sessionCookies = db.collection('SessionCookies');

var accountManager = {
    createSessionCookieForUser: function (userId, accessToken, accessTokenSecret) {
        var newCookie = {
            name: 'sessionCookie',
            userId: userId,
            accessToken: accessToken,
            accessTokenSecret: accessTokenSecret,
        };
        return users.find({ _id: userId }).then(user => {
            user.tokenId = accessToken;
            return users.findOneAndUpdate({ _id: userId }, user)
        }).then(() => {
            return sessionCookies.findOneAndUpdate(newCookie, newCookie, { upsert: true });
        }).then(() => {
            return { msg: '' };
        }).catch(err => {
            return { msg: 'Error: ' + err };
        });
    },
};

module.exports = accountManager;
