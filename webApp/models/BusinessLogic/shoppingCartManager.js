var db = require('../../data/dal/databaseAccess.js');
var shoppingCarts = db.collection('ShoppingCarts');

var shoppingCartManager = {
    getAll: function() {
        return shoppingCarts.find({});
    },
    getById: function(id) {
        return shoppingCarts.findOne({ _id: id });
    },
    getByUserId: function(userId) {
        return shoppingCarts.findOne({ userId: userId });
    },
    createCart: function(newCart) {
        return shoppingCarts.insert(newCart);
    },
    updateCart: function(cartToUpdate) {
        return shoppingCarts.findOneAndUpdate({ _id: cartToUpdate._id }, cartToUpdate);
    },
    addToCartByUserId: function(userId, cartItemToAdd, count) {
        shoppingCartManager
            .getByUserId(userId)
            .then(result => {
                if (!result.items) result.items = [];
                let currItem = result.items.find(x => x._id.toString() === cartItemToAdd._id.toString());
                if (currItem) {
                    let currCount = parseInt(currItem.count);
                    currCount += parseInt(count);
                    currItem.count = currCount;
                    currItem.totalPrice = currItem.count * currItem.price;
                } else {
                    cartItemToAdd.count = count;
                    cartItemToAdd.totalPrice = count * cartItemToAdd.price;
                    result.items = result.items.concat(cartItemToAdd);
                }
                shoppingCartManager.updateCart(result);
            })
            .catch(msg => {
                return msg;
            });
    },

    addArrayToCartByUserId: function(userId, cartItemsToAdd) {
        return shoppingCartManager.addToCartByUserId(userId, cartItemsToAdd);
    },
    removeFromUserCartById: function(userId, itemId) {
        return shoppingCartManager.getByUserId(userId).then(result => {
            let itemToDelete = result.items.find(x => x._id.toString() === itemId.toString());

            if (--itemToDelete.count <= 0) {
                result.items.splice(result.items.findIndex(x => x._id === itemId), 1);
            }
            else {
                itemToDelete.totalPrice = itemToDelete.count * itemToDelete.price;
            }

            return shoppingCartManager.updateCart(result);
        });
    },

    emptyCartForUser: function(userId) {
        return shoppingCartManager.getByUserId(userId).then(result => {
            result.items = [];
            shoppingCartManager.updateCart(result);
        });
    },

    getCartValue: function(userId) {
        shoppingCartManager.getByUserId(userId).then(result => {
            let sum = 0;
            result.items.forEach(x => {
                sum += x.price * x.count;
            });
            return sum;
        });
    },
};

module.exports = shoppingCartManager;
