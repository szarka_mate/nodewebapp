var db = require('../../data/dal/databaseAccess.js');
var menuItems = db.collection('MenuItems');

var menuItemsManager = {
    getAll: function() {
        return menuItems.find({});
    },
    getItemById: function(id) {
        return menuItems.findOne({ _id: id });
    },
    createItem: function(newItem) {
        return menuItems.insert(newItem);
    },
    updateItem: function(itemToUpdate) {
        return menuItems.findOneAndUpdate({ _id: itemToUpdate._id }, { itemToUpdate });
    },
};

module.exports = menuItemsManager;
