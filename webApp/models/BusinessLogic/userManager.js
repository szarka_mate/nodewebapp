var db = require('../../data/dal/databaseAccess.js');
var users = db.collection('Users');
var accountManager = require('../BusinessLogic/accountManager');
var jwt = require('jsonwebtoken');
var guidHelper = require('../../models/BusinessLogic/helpers/guidHelper');

var userManager = {
    getAll: function() {
        return users.find({});
    },
    getUserById: function(id) {
        return users.findOne({ _id: id });
    },
    getUserByEmail: function(email) {
        return users.findOne({ email: email });
    },
    getUserByTokenId: function (tokenId) {
        return users.findOne({ tokenId: tokenId });
    },
    updateUser: function (userToUpdate) {
        return users.findOneAndUpdate({ _id: userToUpdate._id }, userToUpdate);
    },
    createUser: function(newUser) {
        return users.insert(newUser);
    },
    signIn: function (email, password) {
        return userManager.getUserByEmail(email)
            .then(result => {
                if (!result)
                    return { msg: 'wrong email' };

                if (result.password !== password)
                    return { msg: 'wrong password' };

                return { msg: '', user: result };
            });
    },
    isSignedIn: function (email, tokenId) {
        return users.find({ email: email, tokenId: tokenId })
    },
       authorize: (cookie) => {
        var dec = jwt.verify(cookie, 'secret');
        return userManager.isSignedIn(dec.userEmail, dec.accessToken)
            .then((result) => {
                if (result) {
                    return;
                }
                throw new ErrorType("User needs to sign in!");
            })
            .catch(err => {
                throw err;
        })
    }
};


module.exports = userManager;
