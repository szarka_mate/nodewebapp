var jwt = require('jsonwebtoken');

var cookieManager = {
    getDecodedCookie: function (cookie) {
        return jwt.verify(cookie, 'secret');
    }
}

module.exports = cookieManager;